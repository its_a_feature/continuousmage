# conftest.py is a special pytest file that allows configuration options and fixtures for an entire directory

import os
import subprocess

import pytest
import pytest_asyncio
from mythic import mythic, mythic_classes
import asyncio
from os import path, remove
from sys import exit
import json
from uuid import uuid4
import platform
from collections.abc import Callable, Awaitable
from typing import Any


def pytest_addoption(parser):
    add_extra_pytest_options(parser)
    parser.addoption(
        "--server-ip",
        action="store",
        default="127.0.0.1",
        help="specify the IP address of Mythic to use",
    )
    parser.addoption(
        "--username",
        action="store",
        default="mythic_admin",
        help="specify the mythic admin username",
    )
    parser.addoption(
        "--password",
        action="store",
        default="mythic_password",
        help="specify the password for the admin user",
    )
    parser.addoption(
        "--apitoken",
        action="store",
        default=None,
        help="specify an apitoken to use for requests",
    )
    parser.addoption(
        "--server-port",
        action="store",
        default=7443,
        help="specify port for mythic"
    )
    parser.addoption(
        "--payload-types",
        action="store",
        default=None,
        help="specify a comma separated list of payload types to test",
    )
    parser.addoption(
        "--operating-systems",
        action="store",
        default=None,
        help="specify an a comma separated list of operating systems to test",
    )
    parser.addoption(
        "--architectures",
        action="store",
        default=None,
        help="specify a comma separated list of architectures to test",
    )
    parser.addoption(
        "--task-timeout",
        action="store",
        default=None,
        help="specify a timeout to use for tests",
    )
    parser.addoption(
        "--build-timeout",
        action="store",
        default=None,
        help="specify a timeout to use for tests",
    )
    parser.addoption(
        "--callback-display-id",
        action="store",
        default=None,
        help="specify a specific callback to use for command testing",
    )
    parser.addoption(
        "--payload-uuid",
        action="store",
        default=None,
        help="specify an existing payload UUID instead of building a new one"
    )
    parser.addoption(
        "--deployment-mode",
        action="store",
        default="local",
        help="specify the deployment mode as: local or ci-cd",
        choices=("local", "ci-cd")
    )


# Add new markers here - they're used in @pytest.mark.[something]
def pytest_configure(config):
    config.addinivalue_line("markers", "build: mark test as part of building")
    config.addinivalue_line("markers", "agent: mark test as being agent specific")
    config.addinivalue_line("markers", "os: mark test as being os specific")
    config.addinivalue_line("markers", "arch: mark test as being architecture specific")
    config.addinivalue_line("markers", "slow: mark test as being super slow")


# Add additional options here to adjust which @pytest.mark.[value] 's to include or exclude
def add_extra_pytest_options(parser):
    # needed to run @pytest.mark.build
    parser.addoption(
        "--runbuild", action="store_true", default=False, help="run payload building tests"
    )
    parser.addoption(
        "--runslow", action="store_true", default=False, help="run slow tests"
    )


def get_runbuild(config):
    if os.getenv("RUNBUILD"):
        return True
    if config.getoption("--runbuild"):
        return config.getoption("--runbuild")
    return False


def get_runslow(config):
    if os.getenv("RUNSLOW"):
        return True
    if config.getoption("--runslow"):
        return config.getoption("--runslow")
    return False


# Check config flags to determine if we need to skip any tests
def pytest_collection_modifyitems(config, items):
    if not get_runbuild(config):
        # --runbuild not given in cli: skip build tests
        skip_build = pytest.mark.skip(reason="need --runbuild option to run")
        for item in items:
            if "build" in item.keywords:
                item.add_marker(skip_build)
    if not get_runslow(config):
        # --runslow not given in cli: skip slow tests
        skip_slow = pytest.mark.skip(reason="need --runslow option to run")
        for item in items:
            if "slow" in item.keywords:
                item.add_marker(skip_slow)
    supplied_payload_types = get_payload_types(config)
    supplied_operating_systems = get_operating_systems(config)
    supplied_architectures = get_architectures(config)
    for item in items:
        #pytest.set_trace()
        # mark each test based on its agent package
        item.add_marker(pytest.mark.agent(item.module.__package__))
        marks = item.own_markers
        agent_marks = [x.args[0] for x in marks if x.name == 'agent']
        os_marks = [x.args[0] for x in marks if x.name == "os"]
        arch_marks = [x.args[0] for x in marks if x.name == "arch"]
        matched_one_agent = False
        for agent in supplied_payload_types:
            if len(agent_marks) > 0:
                if agent in agent_marks:
                    matched_one_agent = True
            else:
                matched_one_agent = True
        if len(agent_marks) == 0 or len(supplied_payload_types) == 0:
            matched_one_agent = True
        if not matched_one_agent:
            item.add_marker(pytest.mark.skip(reason=f"agent specific test doesn't support any specified agent {supplied_payload_types}"))
        matched_one_os = False
        for supplied_os in supplied_operating_systems:
            if len(os_marks) > 0:
                if supplied_os in os_marks:
                    matched_one_os = True
            else:
                matched_one_os = True
        if len(os_marks) == 0 or len(supplied_operating_systems) == 0:
            matched_one_os = True
        if not matched_one_os:
            item.add_marker(pytest.mark.skip(reason=f"os specific test doesn't support any specified os {supplied_operating_systems}"))
        matched_one_arch = False
        for arch in supplied_architectures:
            if len(arch_marks) > 0:
                if arch in arch_marks:
                    matched_one_arch = True
            else:
                matched_one_arch = True
        if len(arch_marks) == 0 or len(supplied_architectures) == 0:
            matched_one_arch = True
        if not matched_one_arch:
            item.add_marker(pytest.mark.skip(reason=f"arch specific test doesn't support any specified arch {supplied_architectures}"))


@pytest.fixture(scope="session")
async def username(request):
    if os.getenv("USERNAME"):
        return os.getenv("USERNAME")
    if request.config.getoption("--username"):
        return request.config.getoption("--username")
    pytest.fail("Missing username for authenticating to Mythic")


@pytest.fixture(scope="session")
async def password(request):
    if os.getenv("PASSWORD"):
        return os.getenv("PASSWORD")
    if request.config.getoption("--password"):
        return request.config.getoption("--password")
    pytest.fail("Missing password for authenticating to Mythic")


@pytest.fixture(scope="session")
async def server_ip(request):
    if os.getenv("SERVER_IP"):
        return os.getenv("SERVER_IP")
    if request.config.getoption("--server-ip"):
        return request.config.getoption("--server-ip")
    pytest.fail("Missing server-ip for connecting to Mythic")


@pytest.fixture(scope="session")
async def apitoken(request):
    if os.getenv("APITOKEN"):
        return os.getenv("APITOKEN")
    return request.config.getoption("--apitoken")


@pytest.fixture(scope="session")
async def server_port(request):
    if os.getenv("SERVER_PORT"):
        return os.getenv("SERVER_PORT")
    if request.config.getoption("--server-port"):
        return request.config.getoption("--server-port")
    pytest.fail("Missing server-port for connecting to Mythic")


@pytest.fixture(scope="session")
async def task_timeout(request):
    if os.getenv("TASK_TIMEOUT"):
        return int(os.getenv("TASK_TIMEOUT"))
    if request.config.getoption("--task-timeout"):
        return int(request.config.getoption("--task-timeout"))
    return None


@pytest.fixture(scope="session")
async def build_timeout(request):
    if os.getenv("BUILD_TIMEOUT"):
        return int(os.getenv("BUILD_TIMEOUT"))
    if request.config.getoption("--build-timeout"):
        return int(request.config.getoption("--build-timeout"))
    return None


def get_payload_types(config):
    if os.getenv("PAYLOAD_TYPES") and os.getenv("PAYLOAD_TYPES") != "":
        return [x.strip() for x in os.getenv("PAYLOAD_TYPES").split(",")]
    if config.getoption("--payload-types"):
        return [x.strip() for x in config.getoption("--payload-types").split(",")]
    return []


@pytest.fixture(scope="session")
async def payload_types(request):
    return get_payload_types(request.config)


def get_operating_systems(config):
    if os.getenv("OPERATING_SYSTEMS") and os.getenv("OPERATING_SYSTEMS") != "":
        return [x.strip() for x in os.getenv("OPERATING_SYSTEMS").split(",")]
    if config.getoption("--operating-systems"):
        return [x.strip() for x in config.getoption("--operating-systems").split(",")]
    return []


@pytest.fixture(scope="session")
async def operating_systems(request):
    return get_operating_systems(request.config)


def get_architectures(config):
    if os.getenv("ARCHITECTURES") and os.getenv("ARCHITECTURES") != "":
        return [x.strip() for x in os.getenv("ARCHITECTURES").split(",")]
    if config.getoption("--architectures"):
        return [x.strip() for x in config.getoption("--architectures").split(",")]
    return []


@pytest.fixture(scope="session")
async def architectures(request):
    return get_architectures(request.config)


@pytest.fixture(scope="session")
async def deployment_mode(request):
    if os.getenv("DEPLOYMENT_MODE"):
        return os.getenv("DEPLOYMENT_MODE")
    return request.config.getoption("--deployment-mode")


@pytest.fixture
async def random_file_path(request, tmp_path):
    return str(tmp_path / str(uuid4()))


@pytest_asyncio.fixture(scope="session")
async def mythic_instance(request, username, password, server_ip, apitoken, server_port, task_timeout):
    try:
        instance = await mythic.login(
            username=username,
            password=password,
            server_ip=server_ip,
            apitoken=apitoken,
            server_port=server_port,
            timeout=task_timeout
        )
        yield instance
        if path.exists("mythic_schema.graphql"):
            remove("mythic_schema.graphql")
        await asyncio.sleep(1)
    except Exception as e:
        print(f"[-] Failed to log in: {e}\n")
        exit(1)


@pytest.fixture(scope="session")
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest_asyncio.fixture(scope="module")
async def current_payloadtype(request, payload_types):
    #pytest.set_trace()
    if len(payload_types) > 0 and request.module.__package__ not in payload_types:
        pytest.skip(f"{request.module.__package__} is not a selected payload type to execute")
    if request.module.__package__ == "":
        pytest.skip(f"Unknown package name")
    return request.module.__package__


# make_payload is a factory fixture that returns a function allowing you to make a new payload
@pytest_asyncio.fixture(scope="module")
async def make_payload(request, current_payloadtype, mythic_instance, build_timeout):
    async def make(use_payload_build_file: str = None,
                   use_payload_uuid: str = None):
        if use_payload_uuid is not None:
            yield use_payload_uuid
            return
        if use_payload_build_file is None:
            use_payload_build_file = getattr(request.module, "build_file", f"{current_payloadtype}_build.json")
        with open(f"./{current_payloadtype}/supporting-files/{use_payload_build_file}", "r") as f:
            agent_config = json.load(f)
            payload_response = await mythic.create_payload(
                mythic=mythic_instance,
                payload_type_name=agent_config["payload_type"],
                filename=agent_config["filename"],
                operating_system=agent_config["selected_os"],
                commands=agent_config["commands"],
                c2_profiles=agent_config["c2_profiles"],
                build_parameters=agent_config["build_parameters"],
                description=agent_config["description"],
                return_on_complete=True,
                timeout=build_timeout,
                custom_return_attributes="""
                build_phase
                uuid
            """
            )
            if payload_response is None:
                pytest.fail("Failed to build payload due to timeout")
            assert payload_response["build_phase"] == "success"
            yield payload_response["uuid"]
    return make


# payload_uuid is a fixture that happens once per module to generate a payload
# this is if you specifically want the generic payload built for your payload type
@pytest_asyncio.fixture(scope="module")
async def payload_uuid(request, make_payload):
    if os.getenv("PAYLOAD_UUID"):
        return os.getenv("PAYLOAD_UUID")
    if request.config.getoption("--payload-uuid"):
        return request.config.getoption("--payload-uuid")
    if not request.config.getoption("--runbuild"):
        pytest.skip("--runbuild not specified and a new payload is needed")
    # set `build_file` in module to use a specific build file for the module instead of the default generic one
    return await make_payload()


async def get_payload_os(uuid: str, mythic_instance: mythic_classes.Mythic):
    payload = await mythic.get_payload_by_uuid(mythic=mythic_instance,
                                               payload_uuid=uuid,
                                               custom_return_attributes="""
                                                os
                                               """)
    return payload["os"]


# this is for making one-off new callbacks for specific tests and cleaning up after that test is over
@pytest_asyncio.fixture(scope="module")
async def make_callback_display_id(request, mythic_instance, make_payload, task_timeout, tmp_path_factory, deployment_mode):
    async def make(use_payload_uuid: str = None,
                   use_payload_build_file: str = None,
                   # This is called with payload_path=str path of the payload created
                   remote_execution_function: Callable[[str], Awaitable[Any]] = None,
                   # This is called with execution_result=the results of your remote_execute_function call
                   remote_cleanup_function: Callable[[Any], Awaitable[Any]] = None,
                   ):
        temp_path = tmp_path_factory.getbasetemp() / str(uuid4())
        async for new_payload_uuid in make_payload(
            use_payload_uuid=use_payload_uuid,
            use_payload_build_file=use_payload_build_file,
        ):
            max_callback = await mythic.get_all_callbacks(mythic=mythic_instance)
            if len(max_callback) > 0:
                max_callback_id = max_callback[-1]["display_id"]
            else:
                max_callback_id = 0
            payload_os = await get_payload_os(uuid=new_payload_uuid, mythic_instance=mythic_instance)
            with open(temp_path, "wb") as f:
                contents = await mythic.download_payload(mythic=mythic_instance, payload_uuid=new_payload_uuid)
                f.write(contents)
            if deployment_mode == "local":
                platform_os = "macOS" if platform.system() == "Darwin" else platform.system()
                # if the payload os doesn't match our os, we can't run this locally to get a new callback
                if payload_os != platform_os or remote_execution_function is not None:
                    # if the specified remote host to run it on has a different os than the payload then it won't work
                    if remote_execution_function is None:
                        pytest.skip(f"Need to execute payload on remote system, but no remote_execution_function provided.")
                    # if there is no remote user or remote password to use for the remote host then we can't run
                    elif remote_cleanup_function is None:
                        pytest.skip("Need to execute payload on remote system, but no remote_cleanup_function provided")
                    else:
                        yielded_callback = None
                        result = None
                        try:
                            result = await remote_execution_function(temp_path)
                            async for c in mythic.subscribe_custom_query(mythic=mythic_instance, query="""
                    subscription getCallback($payload_uuid: String!, $current_max_id: Int!){
                        callback(where: {display_id: {_gt: $current_max_id}, payload: {uuid: {_eq: $payload_uuid}}}){
                            display_id
                        }
                    }
                    """,
                                                                         variables={"payload_uuid": new_payload_uuid,
                                                                                    "current_max_id": max_callback_id},
                                                                         timeout=task_timeout):
                                if len(c["callback"]) > 0:
                                    yielded_callback = c["callback"][0]["display_id"]
                                    yield c["callback"][0]["display_id"]
                                    break
                        except Exception as e:
                            print(e)
                        finally:
                            if yielded_callback is None:
                                pytest.fail("Never got a new callback")
                            if result is None:
                                pytest.fail("Remote execution function failed to run")
                            await remote_cleanup_function(result)
                            await mythic.execute_custom_query(mythic=mythic_instance,
                                                              query="""
            mutation updateCallbackInformation($callback_display_id: Int!){
                updateCallback(input: {callback_display_id: $callback_display_id, active: false}){
                    status
                    error
                }
            }
                                                              """,
                                                              variables={"callback_display_id": yielded_callback})
                            #await asyncio.sleep(1)
                            return

                os.chmod(temp_path, 0o777)
                proc = subprocess.Popen(temp_path)
                if proc.pid:
                    yielded_callback = None
                    async for c in mythic.subscribe_custom_query(mythic=mythic_instance, query="""
                    subscription getCallback($payload_uuid: String!, $current_max_id: Int!){
                        callback(where: {display_id: {_gt: $current_max_id}, payload: {uuid: {_eq: $payload_uuid}}}){
                            display_id
                        }
                    }
                    """,
                                                                 variables={"payload_uuid": new_payload_uuid,
                                                                            "current_max_id": max_callback_id},
                                                                 timeout=task_timeout):
                        if len(c["callback"]) > 0:
                            yielded_callback = c["callback"][0]["display_id"]
                            yield c["callback"][0]["display_id"]
                            break
                    if yielded_callback is None:
                        pytest.fail("Never got a new callback")
                    proc.terminate()
                    await mythic.execute_custom_query(mythic=mythic_instance,
                                                      query="""
            mutation updateCallbackInformation($callback_display_id: Int!){
                updateCallback(input: {callback_display_id: $callback_display_id, active: false}){
                    status
                    error
                }
            }
                                                              """,
                                                      variables={"callback_display_id": yielded_callback})
                    # sleep for a few seconds to allow the websockets to close properly otherwise pytest closes too fast
                    #await asyncio.sleep(1)
                else:
                    assert 0  # payload failed to start
    return make


@pytest_asyncio.fixture(scope="module")
async def callback_display_id(request, make_callback_display_id, current_payloadtype):
    use_payload_uuid = getattr(request.module, "use_payload_uuid", None)
    use_payload_build_file = getattr(request.module, "use_payload_build_file", f"{current_payloadtype}_build.json")
    remote_execution_function = getattr(request.module, "remote_execution_function", None)
    remote_cleanup_function = getattr(request.module, "remote_cleanup_function", None)
    if os.getenv("CALLBACK_DISPLAY_ID"):
        yield int(os.getenv("CALLBACK_DISPLAY_ID"))
        return
    if request.config.getoption("--callback-display-id"):
        yield int(request.config.getoption("--callback-display-id"))
        return
    async for callback_id in make_callback_display_id(
        use_payload_uuid=use_payload_uuid,
        use_payload_build_file=use_payload_build_file,
        remote_execution_function=remote_execution_function,
        remote_cleanup_function=remote_cleanup_function
    ):
        yield callback_id
    #await asyncio.sleep(1)
    return
