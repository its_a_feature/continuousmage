import os
import pytest
from mythic import mythic
from . import helper_functions
import json


async def wrapped_remote_execution_function(payload_path: str):
    return await helper_functions.remote_execution_function(payload_path=payload_path,
                                                            host=os.getenv("LINUX_X64_REMOTE_HOST"),
                                                            password=os.getenv("LINUX_X64_REMOTE_PASSWORD"),
                                                            user=os.getenv("LINUX_X64_REMOTE_USER"))


@pytest.mark.os("Linux")
@pytest.mark.arch("x64")
async def test_ls(mythic_instance, task_timeout, make_callback_display_id):
    async for callback_id in make_callback_display_id(
            use_payload_build_file="poseidon_linux_x64_build.json",
            # use_payload_uuid="4faadb9c-4a3c-4c6f-b4f8-343965335a29",
            remote_execution_function=wrapped_remote_execution_function,
            remote_cleanup_function=helper_functions.remote_cleanup_function
    ):
        # issue the task and wait for it to finish
        task = await mythic.issue_task(
            mythic=mythic_instance,
            command_name="ls",
            parameters="",
            callback_display_id=callback_id,
            token_id=None,
            wait_for_complete=True,
            custom_return_attributes="""
                id
                status
                completed
                display_id
                callback {
                    id
                }
            """,
            timeout=task_timeout
        )
        # gather all the task's output
        output = await mythic.waitfor_for_task_output(
            mythic=mythic_instance,
            task_display_id=task["display_id"],
            timeout=task_timeout,
        )
        # assert that the output is as expected
        output = json.loads(output)
        assert "name" in output
        assert output["success"]
        # now make sure the data made it into the file browser
        tree_output = await mythic.execute_custom_query(mythic=mythic_instance,
                                                        query="""
           query getFileBrowserData($callback_id: Int!, $task_id: Int!){
            mythictree(where: {callback_id: {_eq: $callback_id}, tree_type: {_eq: "file"}, task_id: {_eq: $task_id}}){
                name_text
                task_id
            }
           }
                                                   """,
                                                        variables={"callback_id": task["callback"]["id"],
                                                                   "task_id": task["id"]})
        assert len(tree_output["mythictree"]) >= len(output["files"]) + 1


use_payload_build_file = "poseidon_linux_x64_build.json"
remote_execution_function = wrapped_remote_execution_function
remote_cleanup_function = helper_functions.remote_cleanup_function


# use a module-wide instance of a callback, callback_display_id, based on the above module parameters
@pytest.mark.os("Linux")
@pytest.mark.arch("x64")
async def test_pwd(mythic_instance, task_timeout, callback_display_id):
    # issue the task and wait for it to finish
    task = await mythic.issue_task(
        mythic=mythic_instance,
        command_name="pwd",
        parameters="",
        callback_display_id=callback_display_id,
        token_id=None,
        wait_for_complete=True,
        custom_return_attributes="""
            id
            status
            completed
            display_id
            callback {
                id
            }
        """,
        timeout=task_timeout
    )
    # gather all the task's output
    output = await mythic.waitfor_for_task_output(
        mythic=mythic_instance,
        task_display_id=task["display_id"],
        timeout=task_timeout,
    )
    # assert that the output is as expected
    assert task["status"] in ["completed", "success"]
    assert len(output) > 0


@pytest.mark.os("Linux")
@pytest.mark.arch("x64")
async def test_ps(mythic_instance, task_timeout, callback_display_id):
    # issue the task and wait for it to finish
    task = await mythic.issue_task(
        mythic=mythic_instance,
        command_name="ps",
        parameters="",
        callback_display_id=callback_display_id,
        token_id=None,
        wait_for_complete=True,
        custom_return_attributes="""
            id
            status
            completed
            display_id
            callback {
                id
            }
        """,
        timeout=task_timeout
    )
    # gather all the task's output
    output = await mythic.waitfor_for_task_output(
        mythic=mythic_instance,
        task_display_id=task["display_id"],
        timeout=task_timeout,
    )
    # assert that the output is as expected
    assert task["status"] in ["completed", "success"]
    assert len(output) > 0
